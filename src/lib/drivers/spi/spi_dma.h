/**
 * @file
 *
 *****************************************************************************
 * @title   spi_dma.h
 * @author  Daniel Schnell (deexschs)
 *
 * @brief   SPI dma functions
 *
 *******************************************************************************
 */

#ifndef __SPI_DMA_H__
#define __SPI_DMA_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "spi_priv.h"

void spi_dma_rx_enable(DiagSPI* spi, bool enable);
void spi_dma_tx_enable(DiagSPI* spi, bool enable);
void spi_dma_rx_init(DiagSPI* spi, void* addr, uint16_t len);
void spi_dma_tx_init(DiagSPI* spi, const void* addr, uint16_t len);
void spi_dma_irq_init(DiagSPI* spi);

#ifdef __cplusplus
}
#endif

#endif /* __SPI_DMA_H__ */

/* EOF */
