/**
 * @file
 *
 *****************************************************************************
 * @title   worker.h
 * @author  Christian Ege <k4230r6@gmail.com
 *
 * @brief   communication interface between the worker task and the rest
 *
 *******************************************************************************
 */

#include <stdint.h>
#include <stddef.h>

#include <FreeRTOS.h>
#include <queue.h>
#include <portmacro.h>

#ifndef WORKER_H__
#define WORKER_H__

typedef enum {
	WORKER_MSG_IDLE = 0,
	WORKER_MSG_NRF24L01_EXTI = 1,
	WORKER_MSG_RTC_ALARM = 2,
	WORKER_MSG_TSL_EVENT = 3,
	WORKER_MSG_MAX
}Worker_Message_ID_t;

typedef union {
	void* usrPtr;   /** @brief unnamed pointer to user data NULL if not needed */
	uint32_t data;	/** @brief unsigned int user data */
} Worker_Message_Payload_t;

/**
 * @struct Worker_Message_t
 * @brief a struct which contains a message and a user pointer to arbitrary  data
 */
typedef struct {
	Worker_Message_ID_t      id;      /** @brief unique ID of the message */
    Worker_Message_Payload_t payload; /** @brief payload of the message */
 }Worker_Message_t;


 /**
 * @brief this returns a handle to the worker message queue
 */
 xQueueHandle worker_comm_get_hdl(void);

 #endif /* WORKER_H__ */
